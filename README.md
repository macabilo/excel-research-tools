# Excel Add-In for WSN Data

Useful Excel macros for processing data

**Sort Data**
The Sort Data tool allows you to re-organize the data into sheets where each sheet plots a variable monitored by the wireless sensor mote. Running this tool in Excel results into a line chart of the data with linear interpolation added for data windows (the data are assumed to be gathered using Goldsmith and Brusey's Linear Spanish Inquisition Protocol).

This data sorting function is currently limited to the raw data format used in the [EEB WSN project by MA Cabilo](https://gitlab.com/planetwsn/contiki-projects). 

**Literature Sort**
The Literature Sort tool allows you to import bib files downloaded from literature databases and sort them into a spreadsheet. Primarily designed for conducting SLR, this tool can also work for any generic literature review work. Can import multiple bib files at a time.

**De-Duplicate**
This tool helps re-organized imported literature by removing duplicate entries that might appear after using the Literature Sort.

